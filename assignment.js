(function() {
    "use strict";
    var formApp = angular.module("formApp", []);
    var formController = function($log) {
        var vm = this;
        vm.item = "";
        vm.list = [];
        vm.addToList = function(event) {
            event = event || window.event;
            if (event.keyCode == 13 && vm.item) {
                vm.list.push({
                    item: vm.item,
                    "completed": false
                });
                vm.item = "";
            }
            ;

        }
    };
    formApp.controller("formController", ["$log", formController]);
})();

