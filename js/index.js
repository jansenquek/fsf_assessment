(function() {
    "use strict";
    var CartApp = angular.module("CartApp", []);
    var CartController = function($log) {
        var vm = this;
        vm.item = "";
        vm.quantity = 1;
        vm.cart = [];
        vm.addToCart = function() {
            vm.cart.push({
                item: vm.item,
                quantity: vm.quantity
            });
            $log.info("Added " + vm.quantity + " item of " + vm.item + " to cart");
            vm.item = "";
            vm.quantity = 1;
        };
    };
    CartApp.controller("CartController", ["$log", CartController]);
})();